#ifndef PLANARMAP_H
#define PLANARMAP_H

#include <vector>
#include <set>
#include <queue>
#include <assert.h>
#include <time.h>

namespace planar {

template<typename Data>
class Edge;	
	
template<typename Data>
class Edge
{
public:
	typedef Edge<Data>* Handle;
	Edge() {}
	~Edge() {}
	Handle getNext()
	{
		return next_;
	}
	Handle getNext(int n)
	{
		if( n < 0 )
		{
			return getPrevious(-n);
		}
		Handle edge = this;
		for(int i=0;i<n;i++)
		{
			edge = edge->getNext();
		}
		return edge;
	}
	void setNext(Handle next)
	{
		next_ = next;
	}
	Handle getPrevious()
	{
		return previous_;
	}
	Handle getPrevious(int n)
	{
		if( n < 0 )
		{
			return getNext(-n);
		}
		Handle edge = this;
		for(int i=0;i<n;i++)
		{
			edge = edge->getPrevious();
		}
		return edge;
	}
	void setPrevious(Handle previous)
	{
		previous_ = previous;
	}
	Handle getAdjacent()
	{
		return adj_;
	}
	void setAdjacent(Handle adj)
	{
		adj_ = adj;
	}
	Handle getRotateCCW()
	{
		return getPrevious()->getAdjacent();
	}
	Handle getRotateCCW(int n)
	{
		if( n < 0 )
		{
			return getRotateCW(-n);
		}
		Handle edge = this;
		for(int i=0;i<n;i++)
		{
			edge = edge->getRotateCCW();
		}
		return edge;		
	}
	Handle getRotateCW()
	{
		return getAdjacent()->getNext();
	}
	Handle getRotateCW(int n)
	{
		if( n < 0 )
		{
			return getRotateCCW(-n);
		}
		Handle edge = this;
		for(int i=0;i<n;i++)
		{
			edge = edge->getRotateCW();
		}
		return edge;		
	}
	int getId() const
	{
		return id_;
	}
	void setId(int id)
	{
		id_ = id;
	}
	Data& data()
	{
		return data_;
	}
private:
	Handle next_;
	Handle previous_;
	Handle adj_;
	int id_;
	Data data_;
};

template<typename Data>
class Map {
public:
	typedef typename Edge<Data>::Handle EdgeHandle;
	typedef typename std::vector<EdgeHandle>::iterator EdgeIterator;
private:
	std::vector<EdgeHandle> edges_;
	EdgeHandle root_;
public:
	Map() {}
	~Map()
	{
		for(EdgeIterator edge = begin();edge!=end();++edge)
		{
			if( *edge )
			{
				delete (*edge);
			}
		}
	}
	
	void copyFrom(Map<Data> & map)
	{
		while( numHalfEdges() < map.numHalfEdges() )
		{
			newEdge();
		}

		for(Map<Data>::EdgeIterator it=map.begin();it!=map.end();it++)
		{
			EdgeHandle edge = getEdge((*it)->getId());
			edge->setNext(getEdge((*it)->getNext()->getId()));
			edge->setPrevious(getEdge((*it)->getPrevious()->getId()));
			edge->setAdjacent(getEdge((*it)->getAdjacent()->getId()));
			edge->data() = (*it)->data();
		}
		setRoot(getEdge(map.getRoot()->getId()));
	}
	
	EdgeIterator begin()
	{
		return edges_.begin();
	}
	EdgeIterator end()
	{
		return edges_.end();
	}
	
	EdgeHandle getRoot()
	{
		return root_;
	}
	void setRoot(EdgeHandle root)
	{
		root_ = root;
	}
	EdgeHandle newEdge()
	{
		edges_.push_back(new Edge<Data>);
		edges_.back()->setId(edges_.size()-1);
		return edges_.back();
	}
	EdgeHandle newDoubleEdge()
	{
		EdgeHandle edge1 = newEdge();
		EdgeHandle edge2 = newEdge();
		edge1->setAdjacent(edge2);
		edge1->setNext(edge2);
		edge1->setPrevious(edge2);
		edge2->setAdjacent(edge1);
		edge2->setNext(edge1);
		edge2->setPrevious(edge1);
		return edge1;
	}
	void deleteEdge(EdgeHandle edge)
	{
		assert(edge == edges_[edge->getId()]);
		if( edge == root_ )
		{
			root_ = 0;
		}
		edges_.back()->setId(edge->getId());
		std::swap(edges_[edge->getId()],edges_.back());
		delete edges_.back();
		edges_.pop_back();
	}
	int numHalfEdges() const 
	{
		return edges_.size();
	}
	void clearMap()
	{
		while( edges_.size() > 0 )
		{
			delete edges_.back();
			edges_.pop_back();
		}
	}
	EdgeHandle getEdge(int id)
	{
		return edges_[id];
	}
	void makePolygon(int n )
	{
		clearMap();
		for(int i=0;i<2*n;i++)
		{
			newEdge();
		}
		for(int i=0;i<n;i++)
		{
			getEdge(i)->setAdjacent(getEdge(i+n));
			getEdge(i)->setNext(getEdge( (i+1)%n ));
			getEdge(i)->setPrevious(getEdge( (i+n-1)%n ));
			getEdge(i+n)->setAdjacent(getEdge(i));
			getEdge(i+n)->setNext(getEdge( (i+n-1)%n + n));
			getEdge(i+n)->setPrevious(getEdge( (i+1)%n + n));
		}
		setRoot(getEdge(0));
	}
	//create parallellogram + grid
	void createParallellogram(int n)
	{
		clearMap();
		
		for(int i=0;i<6*n*n;i++)
		{
		newEdge(); 
		}
		
		for(int i=0;i<6*n*n;i=i+6)
		{
			int j=i/6;//nummer van het vierkant
			//Een vierkantje maken
			getEdge(0+i)->setNext(getEdge(1+i));
			getEdge(1+i)->setPrevious(getEdge(0+i));

			getEdge(1+i)->setNext(getEdge(2+i));
			getEdge(2+i)->setPrevious(getEdge(1+i));

			getEdge(2+i)->setNext(getEdge(0+i));
			getEdge(0+i)->setPrevious(getEdge(2+i));

			getEdge(1+i)->setAdjacent(getEdge(3+i));
			getEdge(3+i)->setAdjacent(getEdge(1+i));

			getEdge(3+i)->setNext(getEdge(4+i));
			getEdge(4+i)->setPrevious(getEdge(3+i));

			getEdge(4+i)->setNext(getEdge(5+i));
			getEdge(5+i)->setPrevious(getEdge(4+i));

			getEdge(5+i)->setNext(getEdge(3+i));
			getEdge(3+i)->setPrevious(getEdge(5+i));
			//Vierkantje plaatsen wrt andere vierkanten
			//j geeft aan welk vierkantje het is (links naar rechts, boven naar beneneden), 
			//(j+1)%n is de kolom, (j-(j)%n)/n de rij
			//horizontale aansluitingen
			if(j%n!=0 and j%n!=n-1)
			{
				getEdge(0+i)->setAdjacent(getEdge(5+(i-6)));
				getEdge(5+i)->setAdjacent(getEdge(0+(i+6)));
				
			}
			else if(j%n==0)
			{
				getEdge(0+i)->setAdjacent(getEdge(5+6*(j+n-1)));
				getEdge(5+i)->setAdjacent(getEdge(0+(i+6)));
			}
			else if(j%n==n-1)
			{
				getEdge(0+i)->setAdjacent(getEdge(5+(i-6)));
				getEdge(5+i)->setAdjacent(getEdge(0+6*(j-n+1)));
			}
			else
			{
				std:: cout<<"ERROR";
			}
			//verticale aansluitingen
			if((j-(j%n))/n!=0 and (j-(j%n))/n!=n-1)
			{
				getEdge(4+i)->setAdjacent(getEdge(2+(i-6*n)));
				getEdge(2+i)->setAdjacent(getEdge(4+(i+6*n)));
			}	
			else if ((j-(j%n))/n==0)
			{
				getEdge(4+i)->setAdjacent(getEdge(2+6*(j+(n-1)*n)));
				getEdge(2+i)->setAdjacent(getEdge(4+(i+6*n)));
			}
			else if ((j-(j%n))/n==n-1)
			{
				getEdge(4+i)->setAdjacent(getEdge(2+(i-6*n)));
				getEdge(2+i)->setAdjacent(getEdge(4+6*(j-(n-1)*n)));
			}
			else{
				std:: cout<< "ERROR";
			}
			
		}
		
		setRoot(getEdge(0));
	}
	EdgeHandle insertEdge(EdgeHandle edge)
	{
		EdgeHandle newedge = newDoubleEdge();
		newedge->getAdjacent()->setNext(edge);
		newedge->setPrevious(edge->getPrevious());
		edge->getPrevious()->setNext(newedge);
		edge->setPrevious(newedge->getAdjacent());
		return newedge;
	}
	void contractVertices(EdgeHandle edge1, EdgeHandle edge2)
	{
		if( edge1 == edge2 )
			return;
		
		EdgeHandle edge1prev = edge1->getPrevious();
		EdgeHandle edge2prev = edge2->getPrevious();
		edge1prev->setNext(edge2);
		edge2prev->setNext(edge1);
		edge1->setPrevious(edge2prev);
		edge2->setPrevious(edge1prev);
	}
	EdgeHandle insertEdge(EdgeHandle edge1, EdgeHandle edge2)
	{
		EdgeHandle newedge = insertEdge(edge1);
		contractVertices( newedge->getNext(), edge2 );
		return newedge;
	}
	std::vector<EdgeHandle> glueEdges(EdgeHandle edge1, EdgeHandle edge2)
	{
		assert( edge1 != edge2 );
		std::vector<EdgeHandle> newfaceEdges;
		if( edge1 != edge2->getNext() )
		{
			edge1->getPrevious()->setNext(edge2->getNext());
			edge2->getNext()->setPrevious(edge1->getPrevious());
			newfaceEdges.push_back(edge2->getNext());
		}
		if( edge2 != edge1->getNext() )
		{
			edge2->getPrevious()->setNext(edge1->getNext());
			edge1->getNext()->setPrevious(edge2->getPrevious());
			newfaceEdges.push_back(edge1->getNext());
		}
		edge1->getAdjacent()->setAdjacent(edge2->getAdjacent());
		edge2->getAdjacent()->setAdjacent(edge1->getAdjacent());
		deleteEdge(edge1);
		deleteEdge(edge2);
		return newfaceEdges;
	}
	void reattachEdge(EdgeHandle edge, EdgeHandle target)
	{
		// change the origin of edge to the origin of target
		// (assuming target and edge are in the same face)
		EdgeHandle adjEdge = edge->getAdjacent();
		if( adjEdge->getNext() != edge )
		{
			adjEdge->getNext()->setPrevious(edge->getPrevious());
			edge->getPrevious()->setNext(adjEdge->getNext());
		}
		edge->setPrevious(target->getPrevious());
		adjEdge->setNext(target);
		target->getPrevious()->setNext(edge);
		target->setPrevious(adjEdge);
	}
	void detachEdge(EdgeHandle edge)
	{
		edge->getPrevious()->setNext(edge->getAdjacent()->getNext());
		edge->getAdjacent()->getNext()->setPrevious(edge->getPrevious());
		edge->setPrevious(edge->getAdjacent());
		edge->getAdjacent()->setNext(edge);
	}
	void removeEdge(EdgeHandle edge)
	{
		EdgeHandle adjEdge = edge->getAdjacent();
		detachEdge(edge);
		detachEdge(adjEdge);
		deleteEdge(edge);
		deleteEdge(adjEdge);
	}
	void insertFace(EdgeHandle edge, int degree)
	{
		EdgeHandle newedge = edge;
		for(int i=0;i<degree-1;i++)
		{
			newedge = insertEdge(newedge)->getNext();
		}
		contractVertices(newedge,edge->getNext());
	}
	EdgeHandle randomVertex()// rnd between 0 and 1
	{
		srand( (unsigned)time( NULL ) );
		float rnd;
		EdgeHandle RandomEdge;
		float coordgetal;
		do
		{
			rnd = (float) rand()/RAND_MAX;
    	
			RandomEdge=getEdge(uniform_int(rnd,numHalfEdges()));
			coordgetal = vertexDegree(RandomEdge);
		}
 		while(rnd<1/coordgetal);
		return RandomEdge;
	}
	int numFaces()
	{
		int num = 0;
		std::vector<bool> visited(numHalfEdges(),false);
		for(int i=0,endi=edges_.size();i<endi;i++)
		{
			EdgeHandle edge = edges_[i];
			if( !visited[edge->getId()] )
			{
				num++;
				while( !visited[edge->getId()] )
				{
					visited[edge->getId()]=true;
					edge = edge->getNext();
				}
			}
		}
		return num;
	}
	int numVertices()
	{
		int num = 0;
		std::vector<bool> visited(numHalfEdges(),false);
		for(int i=0,endi=edges_.size();i<endi;i++)
		{
			EdgeHandle edge = edges_[i];
			if( !visited[edge->getId()] )
			{
				num++;
				while( !visited[edge->getId()] )
				{
					visited[edge->getId()]=true;
					edge = edge->getRotateCCW();
				}
			}
		}
		return num;
	}
	int faceDegree(EdgeHandle edge)
	{
		int degree = 0;
		EdgeHandle faceedge = edge;
		do{
			degree++;
			faceedge = faceedge->getNext();
		} while( faceedge != edge );
		return degree;
	}
	int vertexDegree(EdgeHandle edge)
	{
		int degree = 0;
		EdgeHandle vertedge = edge;
		do{
			degree++;
			vertedge = vertedge->getRotateCCW();
		} while( vertedge != edge );
		return degree;
	}
	bool isPlanarMap(int genus = 1)
	{
		for(int i=0,endi=edges_.size();i<endi;i++)
		{
			if( edges_[i]->getNext()->getPrevious() != edges_[i] ||
				edges_[i]->getPrevious()->getNext() != edges_[i] ||
				edges_[i]->getAdjacent()->getAdjacent() != edges_[i])
				return false;
		}
		return numVertices() - numHalfEdges()/2 + numFaces() == 2 - 2 * genus;
	}
};

}

#endif
