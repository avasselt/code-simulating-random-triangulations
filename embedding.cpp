#include <stdio.h>
#include <vector>
#include <iostream>
#include <time.h>
#include <queue>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <algorithm>


#include "random.h"
#include "planarmap.h"


#include "Eigen/Sparse"
#include "Eigen/Dense"

using namespace planar;

struct EdgeData {
	enum SimplexType { SIMPLEX31, SIMPLEX13, SIMPLEX22, UNSET};
	SimplexType type;
	int vertexId;
	Edge<EdgeData> * nbr; // Potential neighbour in adjacent slice.
	EdgeData() : type(UNSET), nbr(NULL) {};
};

typedef typename Map<EdgeData>::EdgeHandle EdgeHandle;

/*void bindNeighbour(EdgeHandle t, EdgeHandle b)
{
	t->data().nbr = b;
	b->data().nbr = t;
}

void copyTypeAroundFace(EdgeHandle edge)
{
	EdgeHandle curEdge = edge->getNext();
	while( curEdge != edge ) {
		curEdge->data().type = edge->data().type;
		curEdge = curEdge->getNext();
	}	
}

void splitTriangle(Map<EdgeData> & map, EdgeHandle edge)
{
	EdgeHandle otheredge1 = edge->getNext();
	EdgeHandle otheredge2 = edge->getPrevious();
	map.insertFace(edge,3);
	map.insertEdge(otheredge2,otheredge1->getPrevious());
	copyTypeAroundFace(edge);
	copyTypeAroundFace(otheredge1);
	copyTypeAroundFace(otheredge2);
}

void splitTriangle(Map<EdgeData> & map1, Map<EdgeData> & map2, EdgeHandle edge )
{
	assert( edge->data().nbr != NULL );
	splitTriangle(map1,edge);
	splitTriangle(map2,edge->data().nbr);
	EdgeHandle edge1 = edge->getPrevious();
	EdgeHandle edge2 = edge->data().nbr->getPrevious();
	for(int i=0;i<3;i++)
	{
		bindNeighbour(edge1,edge2);
		edge1 = edge1->getAdjacent();
		edge2 = edge2->getAdjacent();
		bindNeighbour(edge1,edge2);
		edge1 = edge1->getNext();
		edge2 = edge2->getNext();
	}
}

bool checkFaceDegrees(Map<EdgeData> & map)
{
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		if( ( (*it)->data().type == EdgeData::SIMPLEX13 && map.faceDegree(*it) != 3 ) ||
			( (*it)->data().type == EdgeData::SIMPLEX31 && map.faceDegree(*it) != 3 ) ||
			( (*it)->data().type == EdgeData::SIMPLEX22 && map.faceDegree(*it) != 4 ) ||
			(*it)->data().type == EdgeData::UNSET )
		{
			std::cerr << (*it)->data().type << ": " << map.faceDegree(*it) << "\n";
			return false;
		}
		if( (*it)->data().type == EdgeData::SIMPLEX22 && (*it)->data().nbr )
		{
			std::cerr << "22~" << (*it)->data().type << ": " << map.faceDegree(*it) << "\n";
			return false;
		}
		if( (*it)->data().nbr && (*it)->data().nbr->data().nbr != (*it) )
		{
			std::cerr << "type " << (*it)->data().type << " nbr issue\n";
			return false;
		}
	}
	return true;
}

EdgeHandle getAdjacent2D(EdgeHandle edge)
{
	// traverse 22-quadrangles until adjacent edge of non-22 type is found
	edge = edge->getAdjacent();
	while( edge->data().type == EdgeData::SIMPLEX22)
	{
		edge = edge->getNext(2)->getAdjacent();
	}
	return edge;
}

bool flipEdge(Map<EdgeData> & map, EdgeHandle edge)
{
	EdgeHandle adjEdge = edge->getAdjacent();
	if( !(edge->data().type == EdgeData::SIMPLEX31 || 
		  edge->data().type == EdgeData::SIMPLEX13) ||
		adjEdge->data().type != edge->data().type ||
		adjEdge == edge->getNext() ||
		adjEdge == edge->getPrevious() )
	{
		return false;
	}

	map.reattachEdge(edge,edge->getPrevious());
	map.reattachEdge(adjEdge,adjEdge->getPrevious());

	return true;
}

bool flipEdge(Map<EdgeData> & map1, Map<EdgeData> & map2, EdgeHandle edge)
{
	EdgeHandle adjEdge = edge->getAdjacent();
	EdgeHandle edge2 = edge->data().nbr;
	EdgeHandle adjEdge2 = edge2->getAdjacent();
	if( adjEdge->data().type != edge->data().type ||
		adjEdge2->data().type != edge2->data().type ||
		adjEdge == edge->getNext() ||
		adjEdge == edge->getPrevious() )
	{
		return false;
	}
	
	map1.reattachEdge(edge,edge->getPrevious());
	map1.reattachEdge(adjEdge,adjEdge->getPrevious());
	map2.reattachEdge(edge2,edge2->getPrevious());
	map2.reattachEdge(adjEdge2,adjEdge2->getPrevious());

	return true;
}

bool flipEdge(std::vector<Map<EdgeData> > & maps, int layer, EdgeHandle edge)
{
	if( edge->data().type == EdgeData::SIMPLEX13 )
	{
		if( edge->data().nbr )
		{
			return flipEdge(maps[layer],maps[(layer+1)%maps.size()],edge);
		} else
		{
			return flipEdge(maps[layer],edge);
		}
	} else if (edge->data().type == EdgeData::SIMPLEX31 )
	{
		if( edge->data().nbr )
		{
			return flipEdge(maps[layer],maps[(layer+maps.size()-1)%maps.size()],edge);
		} else
		{
			return flipEdge(maps[layer],edge);
		}
	}
	return false;
}

void swapEdgeData(EdgeHandle edge1, EdgeHandle edge2)
{
	std::swap(edge1->data(),edge2->data());
	if( edge1->data().nbr )
	{
		edge1->data().nbr->data().nbr = edge1;
	}
	if( edge2->data().nbr )
	{
		edge2->data().nbr->data().nbr = edge2;
	}
}

bool move23(Map<EdgeData> & map, EdgeHandle edge)
{
	EdgeHandle adjEdge = edge->getAdjacent();
	if( !(edge->data().type == EdgeData::SIMPLEX31 || 
		  edge->data().type == EdgeData::SIMPLEX13) || 
		adjEdge->data().type != EdgeData::SIMPLEX22 )
	{
		return false;
	}
	
	EdgeHandle targetEdge = adjEdge->getNext(2);
	map.insertFace(targetEdge,3);
	swapEdgeData(edge,targetEdge);
	swapEdgeData(edge->getNext(),targetEdge->getNext());
	swapEdgeData(edge->getPrevious(),targetEdge->getPrevious());

	EdgeHandle originalSquareEdge1 = adjEdge->getNext(),
		originalSquareEdge2 = adjEdge->getPrevious();
	map.reattachEdge(edge,edge->getPrevious());
	map.reattachEdge(adjEdge,adjEdge->getPrevious(2));
	copyTypeAroundFace( originalSquareEdge1 );
	copyTypeAroundFace( originalSquareEdge2 );	
	
	return true;
}

bool move32(Map<EdgeData> & map, EdgeHandle edge)
{
	if( !(edge->data().type == EdgeData::SIMPLEX31 || 
		  edge->data().type == EdgeData::SIMPLEX13) )
		return false;
	EdgeHandle squareEdge1 = edge->getNext()->getAdjacent(),
		squareEdge2 = edge->getPrevious()->getAdjacent(),
		sepEdge = squareEdge1->getPrevious();
	if( squareEdge1->data().type != EdgeData::SIMPLEX22 || 
		squareEdge2->data().type != EdgeData::SIMPLEX22 || 
		squareEdge2->getNext()->getAdjacent() != sepEdge )
	{
		return false;
	}
	EdgeHandle originalSquareEdge = squareEdge1->getNext();

	map.reattachEdge(sepEdge,squareEdge2->getPrevious());
	map.reattachEdge(sepEdge->getAdjacent(),squareEdge1->getNext(2));
	swapEdgeData(edge,sepEdge);
	swapEdgeData(edge->getNext(),sepEdge->getNext());
	swapEdgeData(edge->getPrevious(),sepEdge->getPrevious());
	
	map.removeEdge(squareEdge1);
	map.removeEdge(squareEdge2);
	copyTypeAroundFace(originalSquareEdge);
	
	return true;
}

template<typename RNG>
Map<EdgeData>::EdgeHandle RandomEdge(Map<EdgeData> & map, RNG & rng)
{
	return map.getEdge(uniform_int(rng,map.numHalfEdges()));
}

std::vector<int> countFaceTypes(Map<EdgeData> & map)
{
	std::vector<int> count(4,0);
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		if( (*it)->data().type == EdgeData::SIMPLEX13 )
			count[0]++;
		else if( (*it)->data().type == EdgeData::SIMPLEX31 )
			count[1]++;
		else if( (*it)->data().type == EdgeData::SIMPLEX22 )
			count[2]++;
		else if( (*it)->data().type == EdgeData::BOUNDARY12 || (*it)->data().type == EdgeData::BOUNDARY21 )
			count[3]++;
	}
	return count;
}

int assignVertexIds(Map<EdgeData> & map, EdgeData::SimplexType type)
{
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().vertexId = -1;
	}
	int nextId = 0;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		if( (*it)->data().type == type && (*it)->data().vertexId == -1 )
		{
			EdgeHandle cur = *it;
			do {
				if( cur->data().type == type )
					cur->data().vertexId = nextId;
				
				cur = cur->getAdjacent()->getNext();
				if( cur->data().type == EdgeData::SIMPLEX22 )
				{
					cur = cur->getNext();
				}
			} while( cur != *it );
			nextId++;
		}
	}	
	return nextId;
}
*/
int assignVertexIds(Map<EdgeData> & map)
{
	// assign ids to all vertices
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().vertexId = -1;
	}
	int nextId = 0;
	EdgeHandle curEdge = map.getRoot();
	do{ 
		if( curEdge->data().vertexId == -1 )
		{
			EdgeHandle vertexEdge = curEdge;
			do {
				vertexEdge->data().vertexId = nextId;
				vertexEdge = vertexEdge->getAdjacent()->getNext();
			} while (vertexEdge != curEdge);
			nextId++;
		}
		curEdge = curEdge->getNext();
	} while( curEdge != map.getRoot() );
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		if( (*it)->data().vertexId == -1 )
		{
			EdgeHandle vertexEdge = *it;
			do {
				vertexEdge->data().vertexId = nextId;
				vertexEdge = vertexEdge->getAdjacent()->getNext();
			} while (vertexEdge != *it);
			nextId++;
		}
	}
	return nextId;
}
/*
EdgeHandle findVertex(EdgeHandle startEdge)
{
	std::set<EdgeHandle> visited;
	std::queue<EdgeHandle> queue;
	queue.push(startEdge);
	queue.push(startEdge->getNext());
	queue.push(startEdge->getPrevious());
	visited.insert(startEdge);
	visited.insert(startEdge->getNext());
	visited.insert(startEdge->getPrevious());
	
	EdgeHandle curEdge = NULL;
	while( !queue.empty() )
	{
		EdgeHandle edge = queue.front()->getAdjacent();
		queue.pop();
		
		if( visited.insert( edge ).second  )
		{
			if( edge->data().type == EdgeData::SIMPLEX22 )
			{
				curEdge = edge->getNext();
				break;
			}
			queue.push(edge->getNext());
			queue.push(edge->getPrevious());
			visited.insert(edge->getNext());
			visited.insert(edge->getPrevious());
		}
	}
	
	assert( curEdge != NULL);
	
	while( curEdge->data().type == EdgeData::SIMPLEX22 )
	{
		curEdge = curEdge->getNext(2)->getAdjacent();
	}
	return curEdge;
}

template<typename RNG>
std::vector<uint64_t> performMoves(std::vector<Map<EdgeData> > & maps, double flipratio, double weight22, RNG & rng, uint64_t num, bool zerolayers )
{
	std::vector<uint64_t> succes(3,0);
	while(succes[0] + succes[1] + succes[2] < num){
		int layer = uniform_int(rng,maps.size());
		double p = uniform_real(rng);
		EdgeHandle moveEdge = RandomEdge(maps[layer],rng);
		if( p < flipratio )
		{
			if( !(zerolayers && moveEdge->data().type == EdgeData::SIMPLEX13) && flipEdge(maps,layer,moveEdge) )
			{
				succes[0]++;
			}
		}else if( 1.0-p < (1.0-flipratio)/(1.0 + weight22) )
		{
			if( move32(maps[layer],moveEdge) )
			{
				succes[1]++;
			}
		}else
		{
			if( move23(maps[layer],moveEdge) )
			{
				succes[2]++;
			}
		}
	}
	for(int i=0,endi=maps.size();i<endi;++i)
	{
		if( !maps[i].getRoot() )
		{
			// root edge has been deleted, set it to a random edge
			maps[i].setRoot( RandomEdge(maps[i],rng) );
		}
	}	
	return succes;
}

template<typename RNG>
std::vector<uint64_t> performSweep(std::vector<Map<EdgeData> > & maps, double flipratio, double weight22, RNG & rng, int num=1, bool zerolayers=false )
{
	// determine total number of edges in the configuration
	uint64_t totaledges = 0;
	for(int i=0,endi=maps.size();i<endi;++i)
	{
		totaledges += maps[i].numHalfEdges()/2;
	}
	// a sweep is defined as a sequence of totaledges succesful moves
	return performMoves(maps,flipratio,weight22,rng,num * totaledges, zerolayers);
}

void generateMinimalTorusConfiguration(Map<EdgeData> & map)	
{
	// The minimal configuration consists of 2 31-simplices, 2 13-simplices,
	// and 2 22-simplices.
	std::vector<EdgeHandle> faceedges;
	map.makePolygon(3);
	faceedges.push_back(map.getRoot());
	faceedges.back()->data().type = EdgeData::SIMPLEX31;
	faceedges.push_back(map.getRoot()->getAdjacent());
	faceedges.back()->data().type = EdgeData::SIMPLEX31;
	map.insertFace(faceedges.back(),3);
	faceedges.push_back(map.getRoot()->getNext()->getAdjacent());
	faceedges.back()->data().type = EdgeData::SIMPLEX22;
	map.insertEdge(faceedges.back(),faceedges.back()->getPrevious());
	faceedges.push_back(faceedges.back()->getNext());
	faceedges.back()->data().type = EdgeData::SIMPLEX22;
	map.insertEdge(faceedges.back(),faceedges.back()->getNext());
	EdgeHandle oppositeEdge = map.getRoot()->getPrevious()->getAdjacent();
	map.insertEdge(faceedges.back(),oppositeEdge->getNext());
	map.insertEdge(faceedges.back()->getNext(),oppositeEdge);
	faceedges.push_back(oppositeEdge->getNext()->getAdjacent());
	faceedges.back()->data().type = EdgeData::SIMPLEX13;
	map.insertEdge(faceedges.back(),faceedges.back()->getNext(2));
	faceedges.push_back(faceedges.back()->getPrevious()->getAdjacent());
	faceedges.back()->data().type = EdgeData::SIMPLEX13;
	
	for(int i=0;i<6;i++)
	{
		copyTypeAroundFace(faceedges[i]);
	}
}

void setNbrOnMinimalTorusConfiguration(Map<EdgeData> & top, Map<EdgeData> & bottom)
{
	// will identify a 31-type edge on top with 13-type edge on bottom
	EdgeHandle edgeTop = top.getRoot();
	EdgeHandle edgeBottom = bottom.getRoot()->getNext()->getAdjacent()
		->getPrevious()->getAdjacent()->getNext()->getAdjacent();
	
	for(int j=0;j<2;j++)
	{
		for(int i=0;i<3;i++)
		{
			//std::cout << "(i,j)=(" << i << "," << j << ")\n";
			assert( edgeTop->data().type == EdgeData::SIMPLEX31 );
			assert( edgeBottom->data().type == EdgeData::SIMPLEX13 );
			bindNeighbour(edgeTop,edgeBottom);
			edgeTop = edgeTop->getNext();
			edgeBottom = edgeBottom->getNext();
		}
		edgeTop = edgeTop->getAdjacent();
		edgeBottom = edgeBottom->getAdjacent();
	}
}

bool exportMaps(std::vector<Map<EdgeData> > & maps, std::string outputfile)
{
	std::ofstream file(outputfile.c_str());
	
	if( !file ) return false;
	
	int numlayers = maps.size();
	file << numlayers << "\n";
	for(int i=0;i<numlayers;i++)
	{
		file << maps[i].numHalfEdges() << "\n"; 
	}
	for(int i=0;i<numlayers;i++)
	{
		file << maps[i].getRoot()->getId()+1 << "\n";
		for(Map<EdgeData>::EdgeIterator it=maps[i].begin();it!=maps[i].end();it++)
		{
			file << (*it)->getNext()->getId()+1 << " " 
				 << (*it)->getAdjacent()->getId()+1 << " "
				 << static_cast<int>((*it)->data().type) << " "
				 << ( (*it)->data().type == EdgeData::SIMPLEX22 || !(*it)->data().nbr ? 
					-1 : (*it)->data().nbr->getId()+1 ) << "\n";
		}
	}
	return true;
}

struct importConfiguration {
	int numlayers;
	int boundary;
	int numtriangles;
	bool periodic;
};

bool importMaps(std::string inputfile, int maxlayers, std::vector<Map<EdgeData> > & maps)
{
	std::ifstream file(inputfile.c_str());
	
	if( !file )
		return false;
	
	maps.clear();
	
	int numlayers;
	file >> numlayers;

	maps.resize( std::min(numlayers,maxlayers) );
	
	for(int i=0;i<numlayers;++i)
	{
		int size;
		file >> size;
		
		if( i < maxlayers )
		{
			while( maps[i].numHalfEdges() < size )
			{
				maps[i].newEdge();
			}
		}
	}
	
	numlayers = std::min(numlayers,maxlayers);
	
	for(int i=0;i<numlayers;++i)
	{
		int root;
		file >> root;
		maps[i].setRoot(maps[i].getEdge(root-1));
		for(Map<EdgeData>::EdgeIterator it=maps[i].begin();it!=maps[i].end();it++)
		{
			int next, adj, nbr, typenumber;
			EdgeData::SimplexType type;
			file >> next >> adj >> typenumber >> nbr;
			EdgeHandle nextEdge = maps[i].getEdge(next-1);
			(*it)->setNext( nextEdge );
			nextEdge->setPrevious( *it );
			(*it)->setAdjacent( maps[i].getEdge(adj-1) );
			type = static_cast<EdgeData::SimplexType>(typenumber);
			(*it)->data().type = type;
			if( type == EdgeData::SIMPLEX31 && nbr != -1 )
			{
				(*it)->data().nbr = maps[(i+numlayers-1)%numlayers].getEdge(nbr-1);
			} else if( type == EdgeData::SIMPLEX13 && nbr != -1 )
			{
				(*it)->data().nbr = maps[(i+1)%numlayers].getEdge(nbr-1);
			} else
			{
				(*it)->data().nbr = NULL;
			} 
		}
	}
	return true;
}

template<typename RNG>
bool adaptMaps(std::vector<Map<EdgeData> > & maps, int numlayers, bool periodic, int numtriangles, RNG & rng)
{
	int size = maps.size();
	if( periodic && size != numlayers ) //if( size < numlayers || (periodic && size != numlayers ) )
	{
		std::cout << "Not possible to adapt size\n";
		return false;
	}
	
	if( !periodic )
	{
		while( size < numlayers )
		{
			// double size by gluing a reflection
			int addlayers = std::min(numlayers-size,size);
			for(int i=0;i<addlayers;++i)
			{
				maps.resize(maps.size()+1);
				maps.back().copyFrom(maps[size-1-i]); 
				
				for(Map<EdgeData>::EdgeIterator it=maps.back().begin();it!=maps.back().end();it++)
				{
					// reflect type vertically
					if( (*it)->data().type == EdgeData::SIMPLEX31 )
						(*it)->data().type = EdgeData::SIMPLEX13;
					else if( (*it)->data().type == EdgeData::SIMPLEX13 )
						(*it)->data().type = EdgeData::SIMPLEX31;
					
					// update neighbour information
					if( (*it)->data().type == EdgeData::SIMPLEX31 ) 
					{
						bindNeighbour(*it, maps[size+i-1].getEdge( i==0 ? (*it)->getId() : (*it)->data().nbr->getId() ) );
					}
					if( i==addlayers-1 && (*it)->data().type == EdgeData::SIMPLEX13  ) 
					{
						(*it)->data().nbr = NULL;
					}
				}
			}
			size += addlayers;
			std::cout << "Added " << addlayers << " via vertical reflection.\n";
		}
	}
				
	std::vector<int> curvolume(numlayers+1,0);	
	for( int i=0;i<numlayers;++i)
	{
		count = countFaceTypes(maps[i]);
		curvolume[i] = count[1]/3;
		curvolume[i+1] = count[0]/3;
	}
	// split triangles until desired number is reached
	for(int i=0;i<numlayers;++i)
	{
		if( curvolume[i] < numtriangles )
		{
			std::cout << "Layer " << i << " extended to " << numtriangles << " triangles.\n";
		}
		while( curvolume[i] < numtriangles || curvolume[i+1] < numtriangles )
		{
			EdgeHandle edge = RandomEdge(maps[i],rng);
			if( edge->data().type == EdgeData::SIMPLEX13 && curvolume[i+1] < numtriangles )
			{
				if( !periodic && i == numlayers-1 )
					splitTriangle(maps[i],edge);
				else	
					splitTriangle(maps[i],maps[(i+1)%numlayers],edge);
				curvolume[i+1]+=2;
				if( periodic && i == numlayers-1 )
				{
					curvolume[0]+=2;
				}
			} else if( edge->data().type == EdgeData::SIMPLEX31 && curvolume[i] < numtriangles )
			{
				if( !periodic && i == 0 )
					splitTriangle(maps[i],edge);
				else
					splitTriangle(maps[i],maps[(i+numlayers-1)%numlayers],edge);
				curvolume[i]+=2;
				if( periodic && i == 0)
				{
					curvolume[numlayers]+=2;
				}
			}
		}
	}
	return true;
}

void generateInitialTorusMaps(std::vector<Map<EdgeData> > & maps, int numlayers, bool periodic )
{
	maps.resize(numlayers);

	for( int i=0;i<numlayers;++i)
	{
		generateMinimalTorusConfiguration(maps[i]);
	}
	
	for(int i=0;i<numlayers-1;++i)
	{
		setNbrOnMinimalTorusConfiguration(maps[i+1],maps[i]);
	}
	if( periodic )
	{
		setNbrOnMinimalTorusConfiguration(maps[0],maps[numlayers-1]);
	}
}
*/
std::vector<Eigen::Triplet<double> > laplacianRules(Map<EdgeData> & map)
{
	// construct the rules of the laplacian matrix on allvertices
	std::vector<Eigen::Triplet<double> > laplacianrules;
	laplacianrules.reserve(4 * map.numHalfEdges() );
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		int id1 = (*it)->data().vertexId,
			id2 = (*it)->getAdjacent()->data().vertexId;

		if( id2 != id1 )
		{
			laplacianrules.push_back( Eigen::Triplet<double>(id1,id2, -1.0) );
			laplacianrules.push_back( Eigen::Triplet<double>(id1,id1,  1.0) );
		}
	}
	return laplacianrules;
}

/*
double fillLogAreaHistogram(Histogram<double> & hist, Map<EdgeData> & map, Eigen::VectorXd & harm1, Eigen::VectorXd & harm2 )
{
	double totalarea = 0.0;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		// only make measurement if this edge has the lowest id in face
		if( (*it)->getNext()->getId() < (*it)->getId() ||
			(*it)->getNext(2)->getId() < (*it)->getId() ||
			(*it)->getNext(3)->getId() < (*it)->getId() )
		{
			continue;
		}
			
		double area = 0.0;
		{
			Eigen::Matrix2d m;
			m << harm1( (*it)->getNext()->getId() ) , harm1( (*it)->getId() ) 
			  , harm2( (*it)->getNext()->getId() ) , harm2( (*it)->getId() );
			area = std::abs(m.determinant()/2);
		}
		if( (*it)->data().type == EdgeData::SIMPLEX22 )
		{
			Eigen::Matrix2d m;
			m << harm1( (*it)->getPrevious(2)->getId() ) , harm1( (*it)->getPrevious()->getId() ) 
			  , harm2( (*it)->getPrevious(2)->getId() ) , harm2( (*it)->getPrevious()->getId() );
			area += std::abs(m.determinant()/2);
		}
		if( area > 0.0 )
		{
			hist.Insert(std::log(area));
			totalarea += area;
		}
	}
	return totalarea;
}

*/
enum SpanningType { UNSET, PRIMAL, DUAL, CYCLE };

// Given a map of genus g, the following function determines 2*g closed discrete (differential) 1-forms
// forms[0], ..., forms[2*g-1] on the dual map, which together span the cohomology class of 1-forms. 
// On the torus these forms are useful for the following reason: for any closed path on the dual map, 
// i.e. a (cyclic) sequence of triangles such that consecutive pairs share an edge, one may compute the
// discrete integral of forms[0] (resp. forms[1]) along the path, i.e. the sum of the values forms[0][edge]
// for each edge crossing the path (from right to left), and the answer will be an integer telling how
// often the path winds around the first cycle (resp. second cycle) of the torus. In particular, a closed
// path is contractible if and only if both discrete integrals vanish.

void findCohomologyBasis(Map<EdgeData> & map, std::vector<std::vector<int> > & forms)
{
	std::vector<SpanningType> spanning(map.numHalfEdges(),UNSET);
	std::vector<bool> visited(map.numHalfEdges(),false);
	
	// determine a spanning tree of the primal map
	// starting at the origin of the root edge
	std::queue<EdgeHandle> queue;
	EdgeHandle curEdge = map.getRoot();
	do {
		visited[curEdge->getId()] = true;
		queue.push(curEdge);
		curEdge = curEdge->getRotateCW();
	} while( curEdge != map.getRoot() );
	while( !queue.empty() )
	{
		curEdge = queue.front();
		queue.pop();
		
		EdgeHandle nbrEdge = curEdge->getAdjacent();
		if( !visited[ nbrEdge->getId() ] )
		{
			// mark the edges in the exploration tree by PRIMAL
			spanning[ nbrEdge->getId() ] = PRIMAL;
			spanning[ curEdge->getId() ] = PRIMAL;
			
			EdgeHandle otherEdge = nbrEdge;
			do{ 
				visited[ otherEdge->getId() ] = true;
				queue.push( otherEdge );
				otherEdge = otherEdge->getRotateCW();
			} while( otherEdge != nbrEdge );
		}
	}
	
	// now determine a complementary dual tree
	std::fill(visited.begin(),visited.end(),false);
	curEdge = map.getRoot();
	do {
		visited[curEdge->getId()] = true;
		queue.push(curEdge);
		curEdge = curEdge->getNext();
	} while( curEdge != map.getRoot() );
	while( !queue.empty() )
	{
		curEdge = queue.front();
		queue.pop();
		
		EdgeHandle nbrEdge = curEdge->getAdjacent();
		if( !visited[ nbrEdge->getId() ] && spanning[ nbrEdge->getId() ] == UNSET )
		{
			// mark the edges in the exploration tree by PRIMAL
			spanning[ nbrEdge->getId() ] = DUAL;
			spanning[ curEdge->getId() ] = DUAL;
			
			EdgeHandle otherEdge = nbrEdge;
			do{ 
				visited[ otherEdge->getId() ] = true;
				queue.push( otherEdge );
				otherEdge = otherEdge->getNext();
			} while( otherEdge != nbrEdge );
		}
	}
	
	// now there should be precisely 2*genus UNSET edges (i.e. 4*genus
	// UNSET half edges). Let's find them and produce a dual form for each
	forms.clear();
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		if( spanning[(*it)->getId()] == UNSET )
		{
			EdgeHandle cycleEdge1 = (*it), cycleEdge2 = (*it)->getAdjacent();

			spanning[cycleEdge1->getId()] = CYCLE;
			spanning[cycleEdge2->getId()] = CYCLE;
			
			forms.push_back(std::vector<int>(map.numHalfEdges(),0));
			
			// traverse the dual tree from cycleEdge1 to cycleEdge2
			EdgeHandle curEdge = cycleEdge1;
			do{
				if( spanning[curEdge->getId()] == DUAL )
				{
					// traverse dual edge and add 1 to discrete form
					// that crosses tree from right to left
					forms.back()[curEdge->getId()] += 1;
					curEdge = curEdge->getAdjacent();
					forms.back()[curEdge->getId()] -= 1;
				}
				curEdge = curEdge->getNext();
			} while( curEdge != cycleEdge2 );
			forms.back()[cycleEdge2->getId()] += 1;
			forms.back()[cycleEdge1->getId()] -= 1;
		}
	}
}

void findHarmonicFunction(Map<EdgeData> & map, std::vector<int> & closedform, Eigen::VectorXd & function )
{
	int numVertices = assignVertexIds(map);
	
	// determine the co-differential of the closed one-form closedform
	Eigen::VectorXd delta = Eigen::VectorXd::Zero(numVertices);
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		delta( (*it)->data().vertexId ) += closedform[ (*it)->getId() ];
	}
	
	// it can happen that delta is identically zero; in that case constant 0 is the correct solution
	bool iszero = true;
	for(int j=0;j<numVertices;j++)
	{
		if( fabs(delta(j)) > 0.5 )
		{
			iszero = false;
			break;
		}
	}
	if( iszero )
	{
		function = Eigen::VectorXd::Zero(numVertices);
		return;
	}
	
	std::vector<Eigen::Triplet<double> > laplacianrules = laplacianRules(map);
	Eigen::SparseMatrix<double> laplacian(numVertices,numVertices);
	laplacian.setFromTriplets(laplacianrules.begin(),laplacianrules.end());
		
	// precondition matrix
	Eigen::ConjugateGradient<Eigen::SparseMatrix<double> > solver;
	solver.compute(laplacian);
	function = solver.solve(delta);
	if(solver.info()!=Eigen::Success )
	{
		std::cout << "ConjugateGradient solver failed: ";
		std::cout << "iterations = " << solver.iterations() << ", ";
		std::cout << "error = " << solver.error() << "\n";
	}
}

void harmonicForm(Map<EdgeData> & map, std::vector<int> & closedform, Eigen::VectorXd & function, Eigen::VectorXd & harm )
{
	harm = Eigen::VectorXd::Zero(map.numHalfEdges());
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		harm( (*it)->getId() ) = closedform[ (*it)->getId() ] 
			- function( (*it)->data().vertexId ) + function( (*it)->getAdjacent()->data().vertexId );
	}
}
/*

// Implement proper mathematical modulus, such that modulus(D,d) = D - d*Floor( D/d ).
double modulus(double D, double d)
{
	double r = std::fmod(D,d);
	if( r < 0 )
	{
		if( d > 0 ) r += d;
		else r -= d;
	}
	return r;
}

// This function takes a 2d vector representating a complex number in the upper-half plane
// and moves it to the `key-hole-shaped' fundamental domain of the SL(2,Z) modular group.
// See https://en.wikipedia.org/wiki/Modular_group#Tessellation_of_the_hyperbolic_plane
void ToFundamentalDomain( Eigen::VectorXd & x)
{
    // If the y-coordinate happens to be negative, we flip its sign.
	if( x(1) < 0.0 )
		x(1) = -x(1);
	
    // Perform a horizontal integer shift such that the x-coordinate is in [-0.5,0.5)
	x(0) = modulus(x(0) + 0.5,1.0) - 0.5;

	double normsq = x.squaredNorm();
    // Repeatedly apply an inversion followed by a horizontal shift until x
    // is in the desired fundamental domain, i.e. has norm >= 1.
	while( normsq < 1.0 )
	{
		x(0) = modulus( - x(0) / normsq + 0.5, 1.0) - 0.5;
		x(1) = x(1) / normsq;
		normsq = x.squaredNorm();
	}
}


Eigen::VectorXd computeModuli(Eigen::VectorXd & harm1, Eigen::VectorXd & harm2 )
{
	double norm1 = harm1.squaredNorm(), norm2 = harm2.squaredNorm();
	double inner = harm1.dot(harm2);
	Eigen::VectorXd tau(2);
	tau << - inner / norm2, std::sqrt( norm1 * norm2 - inner * inner ) / norm2;
	ToFundamentalDomain(tau);
	return tau;
}

*/

template<typename RNG>
EdgeHandle RandomEdge(Map<EdgeData> & map, RNG & rng)
{
	return map.getEdge(uniform_int(rng,map.numHalfEdges()));
}

bool flipEdge(Map<EdgeData> & map, EdgeHandle edge)
{
	EdgeHandle adjEdge = edge->getAdjacent();
	if( adjEdge == edge->getNext() ||
		adjEdge == edge->getPrevious() )
	{
		// edge is a loop: makes no sense to flip
		return false;
	}

	map.reattachEdge(edge,edge->getPrevious());
	map.reattachEdge(adjEdge,adjEdge->getPrevious());

	return true;
}

int main(int argc, char **argv)
{
	//parameters
	int sizeTorus = 200;
	int numTriangles = 2*sizeTorus*sizeTorus;
	int numsweeps=5;
	std::string filename="5_200_sweeps_plotter";

	Xoshiro256PlusPlus rng(getseed());
	
	//intialise map
	Map<EdgeData> map;
	map.createParallellogram(sizeTorus);
	
	//preform sweeps
	for(int i=0;i<numTriangles*numsweeps;++i)
			{
				EdgeHandle randomEdge = RandomEdge(map,rng);
				flipEdge(map,randomEdge);
			}
	
    std::vector<std::vector<int> > forms;
    // find pair of closed 1-forms spanning the cohomology of the map
    findCohomologyBasis(map,forms);
	//intialise vectors for coordinates
    std::vector<Eigen::VectorXd> coordinates(2);
    std::vector<Eigen::VectorXd> harmonicform(2);
    
	//create output file
	std::ofstream file(filename+".txt");
	if( !file ) std::cout<< "error bij file maken";
	
	//assing vertex numbers
	assignVertexIds(map);
	
	//write results to file
    for(int j=0;j<2;++j)
    {
        findHarmonicFunction(map,forms[j],coordinates[j]);
        harmonicForm(map,forms[j],coordinates[j],harmonicform[j]);
		file << coordinates[j]<<"\n\n";
    }

	for(int j=0;j<2;j++) file << harmonicform[j]<<"\n\n";
	for(int j=0;j<map.numHalfEdges();j++) file<<map.getEdge(j)->data().vertexId<<"\n";
    // then 
	
	file.close();

	return 0;
}
