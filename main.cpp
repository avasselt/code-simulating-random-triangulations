#include <stdio.h>
#include <vector>
#include <iostream>
#include <time.h>
#include <queue>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <algorithm>
#include <cmath>
#include <ctime>

#include "random.h"

#include "planarmap.h"

using namespace planar;

struct EdgeData {
	int vertexId;
	int distance;
	
	EdgeData(){};
};

typedef typename Map<EdgeData>::EdgeHandle EdgeHandle;

// perform edge flip in map (returns true if succesful)
bool flipEdge(Map<EdgeData> & map, EdgeHandle edge)
{
	EdgeHandle adjEdge = edge->getAdjacent();
	if( adjEdge == edge->getNext() ||
		adjEdge == edge->getPrevious() )
	{
		// edge is a loop: makes no sense to flip
		return false;
	}

	map.reattachEdge(edge,edge->getPrevious());
	map.reattachEdge(adjEdge,adjEdge->getPrevious());

	return true;
}
// subdivide triangle to which edge belongs into three triangles 
// (with one new vertex in the center of triangle)
void splitTriangle(Map<EdgeData> & map, EdgeHandle edge)
{
	EdgeHandle otheredge1 = edge->getNext();
	EdgeHandle otheredge2 = edge->getPrevious();
	map.insertFace(edge,3);
	map.insertEdge(otheredge2,otheredge1->getPrevious());
}

// Assign vertex ids to the edges in the map and return number of vertices.
int assignVertexIds(Map<EdgeData> & map)
{
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().vertexId = -1;
	}
	
	int nextId = 0;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		if( (*it)->data().vertexId == -1 )
		{
			EdgeHandle vertexEdge = *it;
			do {
				vertexEdge->data().vertexId = nextId;
				vertexEdge = vertexEdge->getAdjacent()->getNext();
			} while (vertexEdge != *it);
			nextId++;
		}
	}
	return nextId;
}

// write map to file in simple space-separated format
bool exportMap(Map<EdgeData> & map, std::string outputfile)
{
	std::ofstream file(outputfile.c_str());
	
	if( !file ) return false;
	
	file << map.numHalfEdges() << "\n"; 
	
	file << map.getRoot()->getId() << "\n";
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		file << (*it)->getNext()->getId() << " " 
			 << (*it)->getAdjacent()->getId() << "\n";
	}
	
	return true;
}

//write historgam to text file
bool exportHistogram(std:: vector<int> Histo, int maxdist, int t, int numTrian)
{
	
	std::ofstream file("Histogram"+std::to_string(t)+".txt");

	if( !file ) return false;
	file<< numTrian << " triangles\n";
	file<< "sweep number: "<< t << "\n";
	for(int i=0;i<maxdist;i++)
	{
		file << Histo[i]<< ",";
	}
	return true;
}

// read map from file in simple space-separated format
bool importMap(std::string inputfile, Map<EdgeData> & map)
{
	std::ifstream file(inputfile.c_str());
	
	if( !file )
		return false;
	
	map.clearMap();
	
	int size, root;
	file >> size >> root;
	
	while( map.numHalfEdges() < size )
	{
		map.newEdge();
	}
	map.setRoot(map.getEdge(root));	
	
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		int next, adj;
		file >> next >> adj;
		EdgeHandle nextEdge = map.getEdge(next);
		(*it)->setNext( nextEdge );
		nextEdge->setPrevious( *it );
		(*it)->setAdjacent( map.getEdge(adj) );
	}
	return true;
}

template<typename RNG>
EdgeHandle RandomEdge(Map<EdgeData> & map, RNG & rng)
{
	return map.getEdge(uniform_int(rng,map.numHalfEdges()));
}
template<typename RNG>
EdgeHandle RandomVertex(Map<EdgeData> & map, RNG & rng)
{
	EdgeHandle randomEdge;
	float coordgetal;
	do
	{
		randomEdge=map.getEdge(uniform_int(rng,map.numHalfEdges()));
		coordgetal = map.vertexDegree(randomEdge);
	}
 	while(random_bernoulli(rng, 1/coordgetal)== false);  
	return randomEdge;
}
// Determine graph distance to the vertex corresponding to the starting
// point of origin. The result is saved in data().distance.
std:: vector<int> assignGraphDistance(Map<EdgeData> & map, EdgeHandle origin, int maxdistance)
{
	// initialize all distances to -1
	std::queue<EdgeHandle> queue;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().distance = -1;
	}
	//intializing histogram
	
	std::vector<int> distanceHistogram(maxdistance,0);

	// set distance to 0 on all edges sharing same starting point
	EdgeHandle edge = origin;
	do {
		edge->data().distance = 0;
		
		queue.push(edge);
		edge = edge->getRotateCCW();
	} while( edge != origin);
	
	// do breadth-first search
	while( !queue.empty() )
	{
		edge = queue.front();
		int dist=edge->data().distance;
		distanceHistogram[dist]++;
		queue.pop();
		if( edge->getAdjacent()->data().distance == -1 )
		{
			// the end point of edge has not been visited yet, so
			// assign distance one greater to the corresponding vertex
			EdgeHandle nextEdge = edge->getAdjacent();
			do {
				nextEdge->data().distance = edge->data().distance + 1;
				queue.push(nextEdge);
				nextEdge = nextEdge->getRotateCCW();
			} while( nextEdge != edge->getAdjacent() );			
		}
	}
	return distanceHistogram;
}
//define datetime to differiencate between output files
std::string datetime()
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];

	time (&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer,80,"%d-%m-%Y %H-%M-%S",timeinfo);
	return std::string(buffer);
}

int main(int argc, char **argv)
{
	// initialize random number generator
	Xoshiro256PlusPlus rng(getseed());

	// initialize empty map with edges carrying the data of EdgeData
	Map<EdgeData> map;
	//Parameters
	int sizeTorus;
	int maxdistance; 
	int stepsize; 
	int numRuns;
	int amountsweeps;
	int failsave_time=3600;
	if(argc==6)
	{
		sizeTorus=atoi(argv[1]);//amount of edges in length of parrallellogram
		maxdistance =atoi(argv[2]);//maximum distance in the histogram
		stepsize = atoi(argv[3]);//array of all values t where historgam needs to be taken
		numRuns = atoi(argv[4]);//the amount of runs (with a new triangulasation) being added to the histograms
		amountsweeps = atoi(argv[5]);//the amount of sweeps per run
	}
	else
	{
		sizeTorus=10;//amount of edges in length of parrallellogram
		maxdistance = 20;//maximum distance in the histogram
		stepsize = 15;//array of all values t where historgam needs to be taken
		numRuns = 2;//the amount of runs (with a new triangulasation) being added to the histograms
		amountsweeps = 301;//the amount of sweeps per run
	}
	
	//define times to save progress
	time_t lastsave;
	time_t current_time;
	//shorthands (to make life easier)
	int lengthls = int(amountsweeps/stepsize);//length of listsweeps
	int numTriangles = 2*sizeTorus*sizeTorus;


	int listsweeps[lengthls];//int(amountsweeps/stepsize)
	for (int i=0; i<lengthls; i+=1)
	{
		listsweeps[i]=i*stepsize;
	}
	//make totalhistogram, fill with 0s
	int totalHistogram[lengthls][maxdistance];
	for(int row=0; row<lengthls;row++)
	{
		for(int col=0; col<maxdistance; col++)
		{
			totalHistogram[row][col]=0;
		}
	}

	time (&lastsave);
	for(int r=0;r<numRuns;r++)
	{
		map.createParallellogram(sizeTorus);
		int j=0;
		for(int s=0;s<amountsweeps;++s)
		{
		// flip numTriangles random edges = a single sweep
			for(int i=0;i<numTriangles;++i)
			{
				EdgeHandle randomEdge = RandomEdge(map,rng);
				flipEdge(map,randomEdge);
			}
			//compute distances from random vertex
			std:: vector<int> distanceHistogram = assignGraphDistance(map,RandomVertex(map, rng), maxdistance);
		
			//save to histogram if sweep is desired
			for (int i=0; i<lengthls; i+=1)
			{
				if(listsweeps[i]==s)
				{
					for(int q=0; q<maxdistance; q++)//q geeft nu de afstand aan
					{
						totalHistogram[j][q]=totalHistogram[j][q]+ distanceHistogram[q];
					}
					j++;
					break;
				};
			}
			
		}
		//save file while running the program
		time(&current_time);
		if(current_time-lastsave>failsave_time)
		{
			time(&lastsave);
			std::ofstream file("temphistogram"+std::to_string(numTriangles)+"run"+std::to_string(r)+"time"+ datetime()+".txt");

			if( !file ) std::cout<< "error bij file maken";
			file<< numTriangles << " triangles\n";
			file<< r << " runs\n";
			file<< "sweeps taken at numbers:";
			for (int i = 0; i<lengthls; i++) 
				{file<< listsweeps[i]<<",";}
			file<< "\n";
			for(int s=0; s<lengthls;s++)
			{
				for(int i=0;i<maxdistance;i++)
				{
					file << totalHistogram[s][i]<< " ";
				}
				file << "\n";
			}
			file.close();
		}
	}
	//write histogram to file
	std::ofstream file("totalhistogram"+std::to_string(numTriangles)+"_time_"+datetime()+".txt");

	if( !file ) std::cout<< "error bij file maken";
	file<< numTriangles << " triangles\n";
	file<< numRuns << " runs\n";
	file<< "sweeps taken at numbers:";
	for (int i = 0; i<lengthls; i++) 
    	{file<< listsweeps[i]<<",";}
	file<< "\n";
	for(int s=0; s<lengthls;s++)
	{
		for(int i=0;i<maxdistance;i++)
		{
			file << totalHistogram[s][i]<< " ";
		}
		file << "\n";
	}
	file.close();
	// write map to file
	exportMap(map,"export.txt");

	return 0;
}
